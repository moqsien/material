## 使用 Manjaro Linux 作为开发环境

### 文档

- [Flask Docs](https://dormousehole.readthedocs.io/en/latest/)
- [Django Docs](https://docs.djangoproject.com/zh-hans/2.1/)
- [Vue.js Docs](https://cn.vuejs.org/)
- [ElementUI Docs](https://element.eleme.cn/#/zh-CN)

### 软件

- [Anaconda List](https://repo.continuum.io/archive/)
- [Anaconda Home](https://www.anaconda.com/distribution/)
- [VSCode](https://code.visualstudio.com/)
- [kite](https://www.kite.com/)
- [DBeaver](https://dbeaver.com/download/)
- [Charles](https://www.charlesproxy.com/download/)
- Sublime Text 3
- Typora
- [LiteIDE X](http://liteide.org/cn/)
- WeChat Electron
- QQ for Linux
- WPS for Liux
- Baidunetdisk
- ShadowSocks Qt5
- SmartGit
- PostMan
- Typora
- Deepin Terminal
- Other Deepin Tools
